require 'time_difference'

module Progress
    def self.display(start_time, start_url, collect_count, queue_count, thread_count)
        time_diff = TimeDifference.between(start_time, Time.now).in_each_component
        hour = time_diff[:hours].to_i % 24
        min  = time_diff[:minutes].to_i % 60
        sec  = time_diff[:seconds].to_i % 60
        time_diff_str = "%0.2d:%0.2d:%0.2d" % [hour, min, sec]

        $stdout.flush
        print "Сайт: " + start_url +
            ", Собрано: " + collect_count.to_s +
            ", В очереди: " + queue_count.to_s +
            ", Кол-во потоков: " + thread_count.to_s +
            ", Время: " + time_diff_str +
            "\r"
    end
end

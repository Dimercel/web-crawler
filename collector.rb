require './filters'

module Collector

    def self.getCollector(id)
        case id
        when 'onlyurl'
            return Collector::OnlyURL.new()
        else
            return nil
        end
    end

    class OnlyURL
        def collect(info)
            result = {}

            if Filter::Normal.filter(info)
                result[:url] = info.url
                return result
            end

            return nil
        end

    end
end

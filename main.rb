require 'uri'
require 'eventmachine'
require 'em-http'
require 'mechanize'
require 'pp'
require 'json'
require 'digest/sha1'
require 'time_difference'

require './filters'
require './http-info'
require './navigator'
require './collector'
require './reporter'


@work_dir = File.expand_path(File.dirname(__FILE__))
config_file = File.read(@work_dir + '/config.json')
config = JSON.parse(config_file)

unless Filter::CorrectURL.filter(
       HttpInfo.new('', config['start_url']))

    puts "Не корректно задан стартовый url: #{config['start_url']}"
    exit
end

@nav = Navigator.new(config,
                   Collector::getCollector(config['collector']),
                   Reporter::getReporter(config['reporter']))

def save_progress(path)
    save = {}
    save[:navigator] = @nav.save()

    File.open(path, 'w'){|file| file.write save.to_json}
end

begin
    @nav.run()
rescue Interrupt => e
    save = {}

    file_path = @work_dir + '/' + 
                Addressable::URI.parse(config['start_url']).host + 
                '.save'

    save_progress(file_path)
    puts "Прогресс сохранен в файл #{file_path}"
    exit
end


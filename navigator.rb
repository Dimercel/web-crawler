require 'uri'
require 'eventmachine'
require 'em-http'
require 'mechanize'
require 'digest/sha1'

require './filters'
require './http-info'
require './progress'

class Navigator
    def initialize(config, collector, reporter)
        @config    = config
        @collector = collector
        @reporter  = reporter

        @start_url      = Addressable::URI.parse(config['start_url'])
        @request_opt    = {:head => {'user-agent' => @config['user-agent']}, :redirects => 1}
        @queue          = [{:url => @config['start_url'], :level => 0}]
        @history        = {} #Хранит в себе уже пройденные урлы
        @history[Digest::SHA1.hexdigest(config['start_url']).to_sym()] = nil
        @start_time     = nil
        @max_level      = @config['max_level']
    end

    def save()
        return {
                 :queue => @queue,
                 :history => @history,
                 :start_time => @start_time,
                 :max_level => @max_level,
                 :reporter => @reporter.save()
         }
    end

    def load(data)
        @queue      = data['queue']
        @history    = data['history']
        @start_time = data['start_time']
        @max_level  = data['max_level']
        @reporter.load(data['reporter'])
    end

    def run()
        @start_time = Time.now

        while @queue.length != 0 do
            EventMachine.run do
                multi = EventMachine::MultiRequest.new

                thread_count = @config['thread_count']
                if thread_count > @queue.length
                    thread_count = @queue.length
                end

                part_urls = @queue.shift(thread_count)

                part_urls.each_with_index do |url_info, inx|
                    req = EventMachine::HttpRequest.new(url_info[:url], {:connect_timeout=>30}).get(@request_opt)

                    req.callback do
                        req_info = HttpInfo.new(@start_url.site)

                        req_info.url = url_info[:url]
                        req_info.status = req.response_header.status
                        req_info.header = req.response_header

                        #Не ходим по страницам, куда уже был редирект
                        if isRepeat?(req.last_effective_url) &&
                            req_info.url != @config['start_url']

                            next
                        end

                        if req_info.url != req.last_effective_url.to_s
                            req_info.url = req.last_effective_url.to_s
                        end

                        @history[Digest::SHA1.hexdigest(req_info.url).to_sym()] = nil

                        @reporter.report(@collector.collect(req_info))

                        #если это была не внешняя ссылка, то
                        #добавляем собранные с нее урлы в очередь
                        if !Filter::ExternalRef.filter(req_info)

                            pure_urls = getUrlsForNavigate(getAdresses(req.response))

                            new_urls = []
                            pure_urls.each do |u|
                                new_urls.push({:url=>u,:level=>url_info[:level]+1})
                            end

                            #Фильтруем по уровню вложенности
                            if @max_level == 0
                                @queue.concat(new_urls)
                            elsif url_info[:level] != @max_level
                                @queue.concat(new_urls)
                            end

                        end

                        Progress::display(@start_time,
                                          @start_url,
                                          @reporter.getReportObjCount(),
                                          @queue.length,
                                          @config['thread_count'])
                    end

                    multi.add(inx, req)

                end

                multi.callback do
                    EventMachine.stop
                end
            end

            sleep @config['timeout']
        end

        @reporter.finish
    end

    private

    #Был ли указанный урл в истории
    def isRepeat?(url)
        return @history.has_key?(Digest::SHA1.hexdigest(url).to_sym())
    end

    #Возвращает урлы по которым еще не проходил Навигатор
    def getUrlsForNavigate(urls)
        if urls.length == 0
            return urls
        end

        result = urls

        result.uniq!

        #приводим ссылки к единому формату
        result.map!{|l| getAbsoluteURL(l)}

        #убираем всякий "мусор"
        result.select!{|l| Filter::CorrectURL.filter(HttpInfo.new('',l))}

        #отфильтровываем повторяющиеся ссылки
        result.select!{|l| !isRepeat?(l)}

        #отфильтровываем те, что уже есть в очереди
        result.select!{ |u| @queue.index{ |x| x[:url] == u } == nil }

        return result
    end

    #Возващает адреса всех ссылок на странице
    #response - html-содержимое страницы
    def getAdresses(response)
        page = Mechanize::Page.new(nil,
                                   {'content-type'=>'text/html'},
                                   response,
                                   nil,
                                   Mechanize.new)

        result = []
        page.links.each{|l| result.push(l.href) if l.href != nil }

        return result
    end

    def getAbsoluteURL(url)
        if url.start_with?('http')
            return url
        end

        if url.start_with?("/")
            return @start_url.site + url
        end

        return "#{@start_url.site}/#{url}"
    end

end

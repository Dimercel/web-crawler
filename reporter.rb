require 'robotstxt'

module Reporter

    def self.getReporter(id)
        case id
        when 'sitemap'
            return Reporter::Sitemap.new()
        else
            return nil
        end
    end

    #Оформляет собранную ин-ию как карту сайта. (http://www.sitemaps.org/)
    class Sitemap
        def initialize()
            @work_dir        = File.expand_path(File.dirname(__FILE__))
            @file_paths      =  [ "#{@work_dir}/sitemap.xml" ]
            @max_url_in_file = 50_000
            @urls_collect    = 0
            @check_robots    = true
            @robots_parser   = Robotstxt::Parser.new('*')


            @sitemap_file = File.open(@file_paths[0], 'w',  File::CREAT|File::TRUNC|0664)
        end

        def save()
            return {
                :work_dir => @work_dir,
                :file_paths => @file_paths,
                :max_url_in_file => @max_url_in_file,
                :urls_collect => @urls_collect,
                :check_robots => @check_robots,
                :robots_parser => @robots_parser.robot_id
            }
        end

        def load(data)
            @work_dir = data['work_dir']
            @file_paths = data['file_paths']
            @max_url_in_file = data['max_url_in_file']
            @urls_collect = data['urls_collect']
            @check_robots = data['check_robots']
            @robots_parser = Robotstxt::Parser.new(data['robots_parser'])
            
            @sitemap_file = File.open(@file_paths.last, 'a')
        end

        def report(data)
            return if data == nil

            if @check_robots
                if @urls_collect == 0
                    @robots_parser.get(data[:url])
                end

                #Учитываем ограничения robots.txt относительно
                #адресов страниц
                return unless @robots_parser.allowed?(data[:url])
            end 

            if @urls_collect % @max_url_in_file == 0
                @sitemap_file.puts("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">")
            end

            @sitemap_file.puts("<url>\n  <loc>" + CGI.escapeHTML(data[:url]) + "</loc>\n</url>")

            @urls_collect += 1

            #Достигли ограничения кол-ва урлов
            #в одном файле
            if @urls_collect % @max_url_in_file == 0
                @sitemap_file.puts("</urlset>")

                new_file_path = "#{@work_dir}/sitemap-" +
                                (@urls_collect / @max_url_in_file).to_i.to_s +
                                '.xml'
                @file_paths.push(new_file_path)

                @sitemap_file = File.open(new_file_path, 'w',  File::CREAT|File::TRUNC|0664)
            end

        end

        def setCheckRobots(active)
            #нельзя менять во время
            #создания отчета
            if @urls_collect == 0
                @check_robots = active
            end
        end

        def RobotsIsCheck?()
            return @check_robots
        end

        def getReportObjCount()
            return @urls_collect
        end

        def finish()
            if @urls_collect != 0
                @sitemap_file.puts("</urlset>")
            end
        end
    end

end

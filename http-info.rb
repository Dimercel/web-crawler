class HttpInfo
    attr_accessor :url, :status, :header, :orig_host

    def initialize(host,url='')
        @orig_host = host
        @url = url
    end

end

require 'em-http'
require 'uri'

module Filter

    class Normal

        def self.filter(http_info)
            if !Filter::CorrectURL.filter(http_info) ||
               Filter::ExternalRef.filter(http_info) ||
               Filter::Anchor.filter(http_info) ||
               http_info.status != 200 ||
               !http_info.header["CONTENT_TYPE"].start_with?('text/html')

                return false
            end

            return true
        end

    end

    class ExternalRef

        def self.filter(http_info)
            return !http_info.url.start_with?(http_info.orig_host)
        end

    end

    class CorrectURL

        def self.filter(http_info)
            unless http_info.url.start_with?('http')
                return false
            end

            return http_info.url =~ URI::DEFAULT_PARSER.regexp[:ABS_URI]
        end

    end

    class Anchor

        def self.filter(http_info)
            if http_info.url =~ /\#[^\/]*/
                return true
            else
                return false
            end
        end

    end

    class NotFound

        def self.filter(http_info)
            return true if http_info.status == 404

            return false
        end

    end
end
